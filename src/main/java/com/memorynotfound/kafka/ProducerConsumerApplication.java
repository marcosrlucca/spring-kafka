package com.memorynotfound.kafka;

import com.memorynotfound.kafka.producer.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerConsumerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ProducerConsumerApplication.class, args);
    }

    @Autowired
    private Sender sender;

    @Override
    public void run(String... strings) throws Exception {
        String data = "Hello World! Welcome to the Accounting's POC";

        Rule rule = new Rule();
        rule.setRule1("rule1");
        rule.setRule2("rule2");
        rule.setRule3("rule3");
        rule.setRule4("rule4");
        rule.setRule5("rule5");
        rule.setRule6("rule6");
        rule.setRule7("rule7");
        rule.setRule8("rule8");
        rule.setRule9("rule9");
        rule.setRule10("rule10");

        Value value = new Value();
        value.setValue1("value1");
        value.setValue2("value2");
        value.setValue3("value3");
        value.setValue4("value4");
        value.setValue5("value5");
        value.setValue6("value6");
        value.setValue7("value7");
        value.setValue8("value8");
        value.setValue9("value9");
        value.setValue10("value10");

        Event event = new Event();
        event.setBaseDate("baseDate");
        event.setPersonDocument("personDocument");
        event.setCompany("company");
        event.setReversal("reversal");
        event.setEvent("event");
        event.setSourceId("sourceid");
        event.setEventId("eventid");
        event.setOriginId("originid");
        event.setTransactionId("transactionid");
        event.setSystem("system");
        event.setCreditUnit("creditunit");
        event.setDebitUnit("debitunit");
        event.setRules(rule);
        event.setValues(value);

        InputData i = new InputData();
        i.setEvent(event);
        i.setEventId(666);
        i.setSource("riscos3");

        sender.send(i);
    }

}