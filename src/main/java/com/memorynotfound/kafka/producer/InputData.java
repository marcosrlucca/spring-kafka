package com.memorynotfound.kafka.producer;

public class InputData {

    private Event event;

    private Integer eventId;

    private String source;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "InputData{" +
                "event=" + event +
                ", eventId=" + eventId +
                ", source='" + source + '\'' +
                '}';
    }
}
