package com.memorynotfound.kafka.producer;

public class Rule {
    private String rule1;
    private String rule2;
    private String rule3;
    private String rule4;
    private String rule5;
    private String rule6;
    private String rule7;
    private String rule8;
    private String rule9;
    private String rule10;

    public String getRule1() {
        return rule1;
    }

    public void setRule1(String rule1) {
        this.rule1 = rule1;
    }

    public String getRule2() {
        return rule2;
    }

    public void setRule2(String rule2) {
        this.rule2 = rule2;
    }

    public String getRule3() {
        return rule3;
    }

    public void setRule3(String rule3) {
        this.rule3 = rule3;
    }

    public String getRule4() {
        return rule4;
    }

    public void setRule4(String rule4) {
        this.rule4 = rule4;
    }

    public String getRule5() {
        return rule5;
    }

    public void setRule5(String rule5) {
        this.rule5 = rule5;
    }

    public String getRule6() {
        return rule6;
    }

    public void setRule6(String rule6) {
        this.rule6 = rule6;
    }

    public String getRule7() {
        return rule7;
    }

    public void setRule7(String rule7) {
        this.rule7 = rule7;
    }

    public String getRule8() {
        return rule8;
    }

    public void setRule8(String rule8) {
        this.rule8 = rule8;
    }

    public String getRule9() {
        return rule9;
    }

    public void setRule9(String rule9) {
        this.rule9 = rule9;
    }

    public String getRule10() {
        return rule10;
    }

    public void setRule10(String rule10) {
        this.rule10 = rule10;
    }
}
